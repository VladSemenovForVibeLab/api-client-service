package ru.semenov.clientservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.clientservice.entity.Client;
import ru.semenov.clientservice.entity.dto.ClientDTO;
import ru.semenov.clientservice.service.ClientService;

import java.lang.ref.Cleaner;
import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {
    @Autowired
    private ClientService clientService;
    @PostMapping("/save")
    public ResponseEntity<Client> saveClient(@RequestBody ClientDTO clientDTO){
        Client client = clientService.saveClient(clientDTO);
        return ResponseEntity.ok().body(client);
    }
    @GetMapping("/listAll")
    public ResponseEntity<List<Client>> listAllClients(){
        List<Client> clients = clientService.listAllClients();
        return ResponseEntity.ok().body(clients);
    }
}
