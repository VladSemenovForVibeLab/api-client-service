package ru.semenov.clientservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.semenov.clientservice.entity.Client;
import ru.semenov.clientservice.entity.dto.ClientDTO;
import ru.semenov.clientservice.repository.ClientRepository;

import java.util.List;

@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepository;

    public Client saveClient(ClientDTO clientDTO) {
        Client client = new Client();
        client.setFirstName(clientDTO.firstName());
        client.setLastName(clientDTO.lastName());
        client.setParticipation(clientDTO.participation());
        if (client == null
                || client.getFirstName() == null
                || client.getLastName() == null
                || client.getParticipation() == null) {
            throw new NullPointerException();
        }
        return clientRepository.save(client);
    }

    public List<Client> listAllClients() {
               List<Client> allClients = clientRepository.findAll();
               if(allClients==null||allClients.isEmpty()){
                   throw new NullPointerException();
               }
               return allClients;
    }
}
