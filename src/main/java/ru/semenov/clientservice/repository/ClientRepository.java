package ru.semenov.clientservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.semenov.clientservice.entity.Client;

public interface ClientRepository extends JpaRepository<Client,Long> {
}
