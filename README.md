# Описание приложения для клиентов

## Описание сущности Client
Класс `Client` представляет клиента и имеет следующие поля:
- `id` (тип данных: `Long`) - идентификатор клиента
- `firstName` (тип данных: `String`) - имя клиента
- `lastName` (тип данных: `String`) - фамилия клиента
- `participation` (тип данных: `BigDecimal`) - участие клиента

## Зависимости
Приложение использует следующие зависимости:
- Spring Boot 3.1.5
- Java 17
- Spring Data JPA
- MariaDB
- Lombok
- Spring Boot Starter Test
- Maven
- Docker 
- Docker Compose

## Описание эндпоинтов
### Создание клиента
**URL:**
```
POST /client/save
```
**Request Body:**
```
{
  "firstName": "Имя клиента",
  "lastName": "Фамилия клиента",
  "participation": "Участие клиента"
}
```
**Response Body:**
```
{
  "id": "Идентификатор клиента",
  "firstName": "Имя клиента",
  "lastName": "Фамилия клиента",
  "participation": "Участие клиента"
}
```

### Получение списка всех клиентов
**URL:**
```
GET /client/listAll
```
**Response Body:**
```
[
  {
    "id": "Идентификатор клиента",
    "firstName": "Имя клиента",
    "lastName": "Фамилия клиента",
    "participation": "Участие клиента"
  },
  ...
]
```

## Описание класса ClientDTO
Класс `ClientDTO` представляет Data Transfer Object (DTO) для клиента и имеет следующие поля:
- `firstName` (тип данных: `String`) - имя клиента
- `lastName` (тип данных: `String`) - фамилия клиента
- `participation` (тип данных: `BigDecimal`) - участие клиента